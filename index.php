<?php
    require 'animal.php';
    
     $sheep = new animal('shaun',2);
     
     echo $sheep->getName(); // "shaun"
     echo "<br>";
     echo $sheep->getLeg(); // 2
     echo "<br>";
     echo $sheep->get_cold_blooded(); // false
    
    echo"<br><br>";
  
    require 'ape.php';

    $sungokong = new ape("kera sakti",4);

    echo $sungokong->getLeg();
    echo "<br>";
    echo $sungokong->yell();// "Auooo"

    echo"<br><br>";

    require 'frog.php';
   
    $kodok = new frog("buduk",2);

    echo $kodok->jump() ; // "hop hop"


?>